#!/bin/bash

CLOUDFLARE_PAGES_URL=`echo $CI_COMMIT_BRANCH | sed -e 's/[^a-zA-Z0-9]/-/g'`

TIMESTAMP=$(date --utc +%FT%TZ)

WEBHOOK_DATA='{
  "username": "",
  "avatar_url": "https://gitlab.com/favicon.png",
  "embeds": [ {
    "color": "3066993",
    "author": {
      "name": "Pipeline Passed",
      "url": "'"$CI_PIPELINE_URL"'",
      "icon_url": "https://gitlab.com/uploads/-/system/user/avatar/'$GITLAB_USER_ID'/avatar.png"
    },
    "title": "'"$GITLAB_USER_LOGIN"'",
    "description": "'"${CI_COMMIT_MESSAGE//$'\n'/ }"'",
    "fields": [
      {
        "name": "Commit",
        "value": "'"[\`$CI_COMMIT_SHORT_SHA\`]($CI_PROJECT_URL/commit/$CI_COMMIT_SHA)"'",
        "inline": true
      },
      {
        "name": "Branch",
        "value": "'"[\`$CI_COMMIT_BRANCH\`]($CI_PROJECT_URL/tree/$CI_COMMIT_REF_NAME)"'",
        "inline": true
      },
      {
        "name": "Preview",
        "value": "https://'"$CLOUDFLARE_PAGES_URL"'.lolmath.pages.dev/",
        "inline": true
      }
      ],
      "timestamp": "'"$TIMESTAMP"'"
    } ]
  }'

for ARG in "$@"; do
  echo -e "[Webhook]: Sending webhook to Discord...\\n";

  (curl --fail --progress-bar -H Content-Type:application/json -d "$WEBHOOK_DATA" "$ARG" \
  && echo -e "\\n[Webhook]: Successfully sent the webhook.") || echo -e "\\n[Webhook]: Unable to send webhook."
done
